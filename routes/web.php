<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
|
|  Dev: D.K.
|  Date start: 09.03.2023
|  Date finish: 10.03.2023
|
|
*/





Auth::routes();

Route::redirect('/register', '/login');
Route::redirect('/home', '/admin');



Route::group(['namespace' => 'App\Http\Controllers\Frontend'], function() {
    Route::get('/', 'AboutController@index')->name('about');
    Route::get('/books', 'BooksController@index')->name('books');
    Route::get('/book/{id}', 'BooksController@single')->name('book-single');
    Route::get('/musings', 'MusingsController@index')->name('musings');
});




Route::group(['middleware'=> ['auth', 'admin'], 'prefix' => '/admin', 'namespace' => 'App\Http\Controllers\Backend'], function() {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::resource('/about/slider', 'AboutSliderController');
    Route::resource('/about/section', 'AboutSectionController');
    Route::resource('/book', 'BooksController');
    Route::resource('/musing', 'MusingController');
    Route::resource('/user', 'UserController');
});
