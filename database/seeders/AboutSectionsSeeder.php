<?php

namespace Database\Seeders;

use App\Models\AboutSection;
use App\Models\User;
use Illuminate\Database\Seeder;

class AboutSectionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'NICOLE DAEDONE',
                'sub_title' => 'A Sought-After Speaker, Author, And Expert In Female Orgasm, Relationships, And Communication',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type .Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type .',
                'path' => '/assets/frontend/images/image.png',
                'section' => 1,
                'status' => 1,
            ],
            [
                'title' => 'THE WAY IT IS',
                'sub_title' => null,
                'description' => 'There’s a thread you follow. It goes among things that change. But it doesn’t change. People wonder about what you are pursuing. You have to explain about the thread. But it is hard for others to see. While you hold it you can’t get lost. Tragedies happen; people get hurt or die; and you suffer and get old. Nothing you do can stop time’s unfolding. You don’t ever let go of the thread. Wiliam Stafford',
                'path' => null,
                'section' => 2,
                'status' => 1,
            ],
            [
                'title' => 'THE STORY THUS FAR',
                'sub_title' => null,
                'description' => 'My life is an endeavor to follow that thread.

I specialize in following it where not many dare to tread. I want to know life biblically, the way a man knows a woman (or other configuration of such). I want to know the water by getting wet. Theory, commandments, concepts leave me empty and not the good kind of emptiness

My driving question is, “is that true?”. Is it true wholly? Where and how is it true? For whom is it true and why? Can it withstand the test of time? Is it true for me as a woman? The last one has taken me into some very off beaten paths. Givens are often no longer givens when I ask this question. The world turns upside down.

My two guiding principles are the ideas that “I’ve come only for this”, whatever is presented before me. It is mine to puzzle, to play, to explore and finally, to love. Which leads me to the how I explore which is to ask, Can I love this? Can I love even this? Who is the I who is loving in this moment? What does love look like here? Does it require a peaceful approach, approval, power, some good old fashioned wrath? And then, what is this? I must leave who I believe myself to be to answer that question. What is this on “its” terms and not on mine. As a free woman I want all things to be free, liberated from any ideas I would impose on them.',
                'path' => '/assets/frontend/images/mathilde-langevin.png',
                'section' => 3,
                'status' => 1,
            ],
            [
                'title' => 'I BELIEVE THAT EVERYTHING, AND I MEAN EVERYTHING, WHEN PROPERLY TENDED TO REVEALS AN UNTOLD BEAUTY.',
                'sub_title' => null,
                'description' => 'I ask what is the highest form of this? What does this need to become that? What does it need cut away in order to become its own unique beauty? How will we know when it is realized?

My absolute specialty is doing this with shadow material, with what we discard or disregard, what we see as trash, people we see as monsters, groups we have seen as outsiders. The mark of my work is that something that has been denigrated is now exalted and not by assimilation but by becoming wholly and uniquely itself -and then fitting into the culture in such a way that it is viewed as invaluable.

I do this by turning every process into a spiritual journey. Or, as one reporter described it, I do everything I do “religiously”. I do it agnostically as well meaning, I am not constrained by the edicts of my known world. I will enter any domain, apprehend it, taste, touch, smell, feel and most of all listen to it until it reveals it’s secrets and then I will offer my gifts to it on it’s terms. That part is vital, that it is not me imposing my will, my idea of what it wants to be but that I am using my powers to access the resources for it to become itself.',
                'path' => null,
                'section' => 4,
                'status' => 1,
            ],
            [
                'title' => 'I CREATE GENRES FROM EXISTING WORLDS BY EXTRACTING THE ESSENCE AND GROWING PHILOSOPHIES FROM THERE.',
                'sub_title' => null,
                'description' => 'I ask what is the highest form of this? What does this need to become that? What does it need cut away in order to become its own unique beauty? How will we know when it is realized?

My absolute specialty is doing this with shadow material, with what we discard or disregard, what we see as trash, people we see as monsters, groups we have seen as outsiders. The mark of my work is that something that has been denigrated is now exalted and not by assimilation but by becoming wholly and uniquely itself -and then fitting into the culture in such a way that it is viewed as invaluable.

I do this by turning every process into a spiritual journey. Or, as one reporter described it, I do everything I do “religiously”. I do it agnostically as well meaning, I am not constrained by the edicts of my known world. I will enter any domain, apprehend it, taste, touch, smell, feel and most of all listen to it until it reveals it’s secrets and then I will offer my gifts to it on it’s terms. That part is vital, that it is not me imposing my will, my idea of what it wants to be but that I am using my powers to access the resources for it to become itself.',
                'path' => '/assets/frontend/images/luli-sosa-benintende.png',
                'section' => 5,
                'status' => 1,
            ],
        ];


        foreach ($data as $key => $item){
            AboutSection::create([
                'title' => $item['title'],
                'sub_title' => $item['sub_title'],
                'description' => $item['description'],
                'path' => $item['path'],
                'section' => $item['section'],
                'status' => $item['status'],
            ]);
        }
    }
}
