<?php

namespace Database\Seeders;

use App\Models\AboutSlider;
use App\Models\Book;
use App\Models\Musing;
use App\Models\User;
use Illuminate\Database\Seeder;

class MusingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => null,
                'sub_title' => null,
                'description' => null,
                'status' => 1,
                'section' => 1,
            ],
            [
                'title' => null,
                'sub_title' => 'February 1, 2023 | thoughts',
                'description' => '<p>“Most things in the world are hype. Most things in the world are oversold and under-deliver. But, in my experience, sex, music, and psychedelics deliver. They are actually ‘better than advertised.’”– Terrance McKenna</p>',
                'status' => 1,
                'section' => 2,
            ],
            [
                'title' => 'THE BOOK IS WRITING YOU',
                'sub_title' => null,
                'description' => '<p>Steven Pressfield talks about the Resistance. A different friend talked about the vulnerability of being led by the inner desire for excellence that shows how much you care about what you are working on. To let that desire lead you to a type of perfection. That you will not let the work see the light of day until you know you’ve given it your all. To bring that level of vulnerability to it.</p>
<br>
<p>JANUARY 31, 2023 | ARTICLE</p>',
                'status' => 1,
                'section' => 3,
            ],
            [
                'title' => null,
                'sub_title' => null,
                'description' => null,
                'status' => 1,
                'section' => 4,
            ],
            [
                'title' => null,
                'sub_title' => null,
                'description' => null,
                'status' => 1,
                'section' => 5,
            ],

            [
                'title' => null,
                'sub_title' => null,
                'description' => null,
                'status' => 1,
                'section' => 6,
            ],
        ];

        foreach ($data as $item){
            Musing::create([
                'title' => $item['title'],
                'sub_title' => $item['sub_title'],
                'description' => $item['description'],
                'status' => $item['status'],
                'section' =>  $item['section'],
            ]);
        }

    }
}
