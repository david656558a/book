<?php

namespace Database\Seeders;

use App\Models\AboutSlider;
use App\Models\User;
use Illuminate\Database\Seeder;

class AboutSlidersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'path' => '/assets/frontend/images/slider1.png',
            ],
            [
                'path' => '/assets/frontend/images/slider2.png',
            ],
            [
                'path' => '/assets/frontend/images/slider3.png',
            ],
            [
                'path' => '/assets/frontend/images/slider4.png',
            ],
        ];

        foreach ($data as $item){
            AboutSlider::create([
                'path' => $item['path'],
            ]);
        }

    }
}
