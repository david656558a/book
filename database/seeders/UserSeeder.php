<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
           'name' => 'admin',
           'type' => 'admin',
           'email' => 'admin@gmail.com',
           'instagram' => 'https://www.instagram.com/nicoledaedone/',
           'password' => \Hash::make('password')
        ]);
    }
}
