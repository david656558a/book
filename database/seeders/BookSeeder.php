<?php

namespace Database\Seeders;

use App\Models\AboutSlider;
use App\Models\Book;
use App\Models\User;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'Sutras for the art & practis of eros',
                'sub_description' => '<p>Sutras for the Art and Practice of Eros is the foundational, complete text for the study and practice of Eros, the path of feminine spirituality. In these never before shared teachings, one finds the path to uncovering the truth within themselves and the journey of their true nature.

                        This volume of the Sutras lays out how the mind, physical body and the various energetic bodies interrelate. Discover a deep dive into the practice of Orgasmic Meditation and the principles that undergird the practice that has helped heal the maladies of thousands. The liberation that is available in the study of Eros, giving us the framework for understanding the descending path of the soul. This volume covers Practice and Liberation.</p>',
                'description' => '<p>I am the woman you have always been terrified of. I don’t remember the time before my father touched me there. I had my first sugar daddy at 16 and a short stint as a prostitute at 28. I was an academic star in the women’s studies department at San Francisco State. I felt so choked by a conventional life as a gallery owner that I tore off the little black dress and pearls and traded it for an LSD guru. I have been beaten, choked, addicted to drugs, money, and power. I have been the ultimate victim. And I will talk about it all.</p>
        <p>And I will tell you this. Though these things happened; I have never been harmed. I live in the underworld and just like heaven, it is also paradise.</p>
        <p>I spent years looking for the cure to my soul’s hunger in LSD, Zen, and destructive behavior. When I discovered the cure in orgasm, I created a curriculum and a movement out of it. I opened centers dedicated to healing through orgasmic meditation all over the world. I pioneered new methods. I got neuroscientists to study it. I watched trauma heal and saw thousands of lives transformed. And the movement continues to grow.</p>
        <p>The day my part in it came crashing down was not the worst day of my life. I’ve had many worse.</p>
        <p>I have been called what many women who own their power have been called: polarizing, manipulative, bossy, vain, power-hungry. All of it and none of it is true.Every woman fears this brash description of herself; the fear of it makes us repulsed by our power and so we snuff out our own light by fitting in, remaining safe, staying meek. But when we crack open the shell of fear, rising in the feminine, not as masculinized females, everything changes. We become free to love and live unconditionally.</p>
        <p>My story is not unique; but the power I take from it is. I have desire. I have hunger. I refuse to scrub it out of my personality, I refuse to be ashamed. Living a life without shame begins by making love, by bringing a sense of intimacy in which we embrace our lives and all around us. We live in a culture that excludes what and who we think is undesirable, unwholesome. Yet I have learned that inside every pathology is beauty. And so I want it all. I want to be present to all of life, unconditionally loving every part of it. What will look extreme to you is normal to me. Not because of dissociation or psychopathology, but because I am living an unconditional life. As I did with orgasmic meditation, I want to infinitely expand this movement to free all women from shame and victimhood. We know by seeing and doing. I will tell you everything I’ve done.</p>',
                'author' => 'By Nicole Daedone',
                'path' => '/assets/frontend/images/book1.png',
                'status' => 1,
            ],
            [
                'title' => 'WOMAN FIRE AND DANGEROUS THINGS',
                'sub_description' => '<p>I am the woman you have always been terrified of. I don’t remember the time before my father touched me there. I had my first sugar daddy at 16 and a short stint as a prostitute at 28. I was an academic star in the women’s studies department at San Francisco State. I felt so choked by a conventional life as a gallery owner that I tore off the little black dress and pearls and traded it for an LSD guru. I have been beaten, choked, addicted to drugs, money, and power. I have been the ultimate victim. And I will talk about it all. And I will tell you this. Though these things happened; I have never been harmed. I live in the underworld and just like heaven, it is also paradise.</p>',
                'description' => '<p>I am the woman you have always been terrified of. I don’t remember the time before my father touched me there. I had my first sugar daddy at 16 and a short stint as a prostitute at 28. I was an academic star in the women’s studies department at San Francisco State. I felt so choked by a conventional life as a gallery owner that I tore off the little black dress and pearls and traded it for an LSD guru. I have been beaten, choked, addicted to drugs, money, and power. I have been the ultimate victim. And I will talk about it all.</p>
        <p>And I will tell you this. Though these things happened; I have never been harmed. I live in the underworld and just like heaven, it is also paradise.</p>
        <p>I spent years looking for the cure to my soul’s hunger in LSD, Zen, and destructive behavior. When I discovered the cure in orgasm, I created a curriculum and a movement out of it. I opened centers dedicated to healing through orgasmic meditation all over the world. I pioneered new methods. I got neuroscientists to study it. I watched trauma heal and saw thousands of lives transformed. And the movement continues to grow.</p>
        <p>The day my part in it came crashing down was not the worst day of my life. I’ve had many worse.</p>
        <p>I have been called what many women who own their power have been called: polarizing, manipulative, bossy, vain, power-hungry. All of it and none of it is true.Every woman fears this brash description of herself; the fear of it makes us repulsed by our power and so we snuff out our own light by fitting in, remaining safe, staying meek. But when we crack open the shell of fear, rising in the feminine, not as masculinized females, everything changes. We become free to love and live unconditionally.</p>
        <p>My story is not unique; but the power I take from it is. I have desire. I have hunger. I refuse to scrub it out of my personality, I refuse to be ashamed. Living a life without shame begins by making love, by bringing a sense of intimacy in which we embrace our lives and all around us. We live in a culture that excludes what and who we think is undesirable, unwholesome. Yet I have learned that inside every pathology is beauty. And so I want it all. I want to be present to all of life, unconditionally loving every part of it. What will look extreme to you is normal to me. Not because of dissociation or psychopathology, but because I am living an unconditional life. As I did with orgasmic meditation, I want to infinitely expand this movement to free all women from shame and victimhood. We know by seeing and doing. I will tell you everything I’ve done.</p>',
                'author' => 'By Nicole Daedone',
                'path' => '/assets/frontend/images/book2.png',
                'status' => 1,
            ],
            [
                'title' => 'Woman | The Uncomfortable Truth',
                'sub_description' => '<p>I am the woman you have always been terrified of. I don’t remember the time before my father touched me there. I had my first sugar daddy at 16 and a short stint as a prostitute at 28. I was an academic star in the women’s studies department at San Francisco State. I felt so choked by a conventional life as a gallery owner that I tore off the little black dress and pearls and traded it for an LSD guru. I have been beaten, choked, addicted to drugs, money, and power. I have been the ultimate victim. And I will talk about it all. And I will tell you this. Though these things happened; I have never been harmed. I live in the underworld and just like heaven, it is also paradise.</p>',
                'description' => '<p>I am the woman you have always been terrified of. I don’t remember the time before my father touched me there. I had my first sugar daddy at 16 and a short stint as a prostitute at 28. I was an academic star in the women’s studies department at San Francisco State. I felt so choked by a conventional life as a gallery owner that I tore off the little black dress and pearls and traded it for an LSD guru. I have been beaten, choked, addicted to drugs, money, and power. I have been the ultimate victim. And I will talk about it all.</p>
        <p>And I will tell you this. Though these things happened; I have never been harmed. I live in the underworld and just like heaven, it is also paradise.</p>
        <p>I spent years looking for the cure to my soul’s hunger in LSD, Zen, and destructive behavior. When I discovered the cure in orgasm, I created a curriculum and a movement out of it. I opened centers dedicated to healing through orgasmic meditation all over the world. I pioneered new methods. I got neuroscientists to study it. I watched trauma heal and saw thousands of lives transformed. And the movement continues to grow.</p>
        <p>The day my part in it came crashing down was not the worst day of my life. I’ve had many worse.</p>
        <p>I have been called what many women who own their power have been called: polarizing, manipulative, bossy, vain, power-hungry. All of it and none of it is true.Every woman fears this brash description of herself; the fear of it makes us repulsed by our power and so we snuff out our own light by fitting in, remaining safe, staying meek. But when we crack open the shell of fear, rising in the feminine, not as masculinized females, everything changes. We become free to love and live unconditionally.</p>
        <p>My story is not unique; but the power I take from it is. I have desire. I have hunger. I refuse to scrub it out of my personality, I refuse to be ashamed. Living a life without shame begins by making love, by bringing a sense of intimacy in which we embrace our lives and all around us. We live in a culture that excludes what and who we think is undesirable, unwholesome. Yet I have learned that inside every pathology is beauty. And so I want it all. I want to be present to all of life, unconditionally loving every part of it. What will look extreme to you is normal to me. Not because of dissociation or psychopathology, but because I am living an unconditional life. As I did with orgasmic meditation, I want to infinitely expand this movement to free all women from shame and victimhood. We know by seeing and doing. I will tell you everything I’ve done.</p>',
                'author' => 'By Nicole Daedone',
                'path' => '/assets/frontend/images/book3.png',
                'status' => 1,
            ],
            [
                'title' => 'The art of soul making',
                'sub_description' => ' <p>I am the woman you have always been terrified of. I don’t remember the time before my father touched me there. I had my first sugar daddy at 16 and a short stint as a prostitute at 28. I was an academic star in the women’s studies department at San Francisco State. I felt so choked by a conventional life as a gallery owner that I tore off the little black dress and pearls and traded it for an LSD guru. I have been beaten, choked, addicted to drugs, money, and power. I have been the ultimate victim. And I will talk about it all. And I will tell you this. Though these things happened; I have never been harmed. I live in the underworld and just like heaven, it is also paradise.</p>',
                'description' => '<p>I am the woman you have always been terrified of. I don’t remember the time before my father touched me there. I had my first sugar daddy at 16 and a short stint as a prostitute at 28. I was an academic star in the women’s studies department at San Francisco State. I felt so choked by a conventional life as a gallery owner that I tore off the little black dress and pearls and traded it for an LSD guru. I have been beaten, choked, addicted to drugs, money, and power. I have been the ultimate victim. And I will talk about it all.</p>
        <p>And I will tell you this. Though these things happened; I have never been harmed. I live in the underworld and just like heaven, it is also paradise.</p>
        <p>I spent years looking for the cure to my soul’s hunger in LSD, Zen, and destructive behavior. When I discovered the cure in orgasm, I created a curriculum and a movement out of it. I opened centers dedicated to healing through orgasmic meditation all over the world. I pioneered new methods. I got neuroscientists to study it. I watched trauma heal and saw thousands of lives transformed. And the movement continues to grow.</p>
        <p>The day my part in it came crashing down was not the worst day of my life. I’ve had many worse.</p>
        <p>I have been called what many women who own their power have been called: polarizing, manipulative, bossy, vain, power-hungry. All of it and none of it is true.Every woman fears this brash description of herself; the fear of it makes us repulsed by our power and so we snuff out our own light by fitting in, remaining safe, staying meek. But when we crack open the shell of fear, rising in the feminine, not as masculinized females, everything changes. We become free to love and live unconditionally.</p>
        <p>My story is not unique; but the power I take from it is. I have desire. I have hunger. I refuse to scrub it out of my personality, I refuse to be ashamed. Living a life without shame begins by making love, by bringing a sense of intimacy in which we embrace our lives and all around us. We live in a culture that excludes what and who we think is undesirable, unwholesome. Yet I have learned that inside every pathology is beauty. And so I want it all. I want to be present to all of life, unconditionally loving every part of it. What will look extreme to you is normal to me. Not because of dissociation or psychopathology, but because I am living an unconditional life. As I did with orgasmic meditation, I want to infinitely expand this movement to free all women from shame and victimhood. We know by seeing and doing. I will tell you everything I’ve done.</p>',
                'author' => 'By Nicole Daedone',
                'path' => '/assets/frontend/images/book1.png',
                'status' => 1,
            ],
        ];

        foreach ($data as $item){
            Book::create([
                'title' => $item['title'],
                'sub_description' => $item['sub_description'],
                'description' => $item['description'],
                'author' => $item['author'],
                'path' =>  $item['path'],
                'status' => $item['status'],
            ]);
        }

    }
}
