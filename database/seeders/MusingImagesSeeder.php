<?php

namespace Database\Seeders;

use App\Models\AboutSlider;
use App\Models\Book;
use App\Models\Musing;
use App\Models\MusingImage;
use App\Models\User;
use Illuminate\Database\Seeder;

class MusingImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'musing_id' => 1,
                'path' => '/assets/frontend/images/musings-main-image.png',
            ],
            [
                'musing_id' => 2,
                'path' => '/assets/frontend/images/no-revisions-d2zvqp3fpro-unsplash%201%20(1).png',
            ],
            [
                'musing_id' => 4,
                'path' => '/assets/frontend/images/book2.png',
            ],
            [
                'musing_id' => 4,
                'path' => '/assets/frontend/images/book-description.png',
            ],
            [
                'musing_id' => 5,
                'path' => '/assets/frontend/images/image-painting.png',
            ],
            [
                'musing_id' => 5,
                'path' => '/assets/frontend/images/book-image.png',
            ],
            [
                'musing_id' => 6,
                'path' => 'assets/frontend/video/The%20Age%20of%20Eros%20-%20Nicole%20Daedone%20LA.mp4',
            ],

        ];

        foreach ($data as $item){
            MusingImage::create([
                'musing_id' => $item['musing_id'],
                'path' => $item['path'],
            ]);
        }

    }
}
