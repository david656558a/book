<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MusingImage extends Model
{
    use HasFactory;

    protected $table = 'musings_image';

    protected $fillable =[
        'musing_id',
        'path',
    ];

    public function musing()
    {
        return $this->belongsTo(Musing::class);
    }

}
