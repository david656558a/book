<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutSlider extends Model
{
    use HasFactory;

    protected $table = 'about_sliders';

    protected $fillable =[
        'path',
    ];
}
