<?php

namespace App\Http\Controllers\Frontend;

use App\Models\AboutSection;
use App\Models\AboutSlider;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AboutController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sliders = AboutSlider::all();
        $sections = AboutSection::query()->where('status', 1)->get();
        return view('frontend.about', compact('sliders', 'sections'));
    }

}
