<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Musing;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class MusingsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $musings = Musing::query()->with('images')->get();
        return view('frontend.musings', compact('musings'));
    }
}
