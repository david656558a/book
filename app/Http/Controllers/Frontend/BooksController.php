<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class BooksController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $books = Book::all();
        return view('frontend.books', compact('books'));
    }


    public function single($id)
    {
        $book = Book::findOrFail($id);
        $books = Book::all();
        return view('frontend.single-book', compact('book', 'books'));
    }
}
