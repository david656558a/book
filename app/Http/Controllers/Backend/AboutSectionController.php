<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Hellper\Hellper;
use App\Models\AboutSection;
use App\Models\AboutSlider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AboutSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $sections = AboutSection::query()->paginate(10);
        return view('backend.dashboard.about.section.index', compact('sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.about.section.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $section = AboutSection::findOrFail($id);
        return view('backend.dashboard.about.section.edit', compact('section'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $section = AboutSection::findOrFail($id);

            $section->title = $request->title;

            if (isset($request->image)){
                $PathForDelete = str_replace('/storage', '', $section->path);
                Storage::delete('/public' . $PathForDelete);
                $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'about' . DIRECTORY_SEPARATOR . 'slider' . DIRECTORY_SEPARATOR);
                $name =  Hellper::upload($request->image, $path);
                $image = "/storage/about/slider/" . $name;
                $section->path = $image;
            }
            if (isset($request->sub_title)){
                $section->sub_title = $request->sub_title;
            }

            $section->status = isset($request->status) ? 1 : 0;
            $section->description = $request->description;

            $section->save();


            DB::commit();
            return redirect()->route('section.index')->with('message', 'Successfully');
        } catch (\Throwable $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $section = AboutSection::findOrFail($id);
        $PathForDelete = str_replace('/storage', '', $section->path);
        Storage::delete('/public' . $PathForDelete);
        $section->delete();
        return redirect()->route('slider.index')->with('message', 'Successfully');
    }

}
