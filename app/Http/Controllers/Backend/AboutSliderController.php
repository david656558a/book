<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Hellper\Hellper;
use App\Models\AboutSlider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AboutSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $sliders = AboutSlider::query()->paginate(10);
        return view('backend.dashboard.about.slider.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.about.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'about' . DIRECTORY_SEPARATOR . 'slider' . DIRECTORY_SEPARATOR);
            $name =  Hellper::upload($request->image, $path);
            $image = "/storage/about/slider/" . $name;
            $slider = AboutSlider::create([
                'path' =>$image
            ]);
            DB::commit();
            return redirect()->route('slider.index')->with('message', 'Successfully');
        } catch (\Throwable $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $slider = AboutSlider::findOrFail($id);
        return view('backend.dashboard.about.slider.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $slider = AboutSlider::findOrFail($id);

            if (isset($request->image)){

                $PathForDelete = str_replace('/storage', '', $slider->path);
                Storage::delete('/public' . $PathForDelete);

                $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'about' . DIRECTORY_SEPARATOR . 'slider' . DIRECTORY_SEPARATOR);
                $name =  Hellper::upload($request->image, $path);
                $image = "/storage/about/slider/" . $name;
                $slider->update([
                    'path' =>$image
                ]);
            }


            DB::commit();
            return redirect()->route('slider.index')->with('message', 'Successfully');
        } catch (\Throwable $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $slider = AboutSlider::findOrFail($id);
        $PathForDelete = str_replace('/storage', '', $slider->path);
        Storage::delete('/public' . $PathForDelete);
        $slider->delete();
        return redirect()->route('slider.index')->with('message', 'Successfully');
    }

}
