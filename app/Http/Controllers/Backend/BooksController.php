<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Hellper\Hellper;
use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $books = Book::query()->paginate(10);
        return view('backend.dashboard.book.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.book.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'book' . DIRECTORY_SEPARATOR);
            $name =  Hellper::upload($request->image, $path);
            $image = "/storage/book/" . $name;

            $book = Book::create([
                'path' =>$image,
                'title' =>$request->title,
                'author' =>$request->author,
                'sub_description' =>$request->sub_description,
                'description' =>$request->description,
                'status' => isset($request->status) ? 1 : 0,
            ]);

            DB::commit();
            return redirect()->route('book.index')->with('message', 'Successfully');
        } catch (\Throwable $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $book = Book::findOrFail($id);
        return view('backend.dashboard.book.edit', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $book = Book::findOrFail($id);

            if (isset($request->image)){
                $PathForDelete = str_replace('/storage', '', $book->path);
                Storage::delete('/public' . $PathForDelete);
                $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'book' . DIRECTORY_SEPARATOR);
                $name =  Hellper::upload($request->image, $path);
                $image = "/storage/book/" . $name;
                $book->path = $image;
            }

            $book->title = $request->title;
            $book->author = $request->author;
            $book->sub_description = $request->sub_description;
            $book->description = $request->description;
            $book->status = isset($request->status) ? 1 : 0;

            $book->save();


            DB::commit();
            return redirect()->route('book.index')->with('message', 'Successfully');
        } catch (\Throwable $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        $PathForDelete = str_replace('/storage', '', $book->path);
        Storage::delete('/public' . $PathForDelete);
        $book->delete();
        return redirect()->route('book.index')->with('message', 'Successfully');
    }
}
