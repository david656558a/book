<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Hellper\Hellper;
use App\Models\Musing;
use App\Models\MusingImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MusingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $musings = Musing::query()->with('images')->paginate(10);
        return view('backend.dashboard.musing.index', compact('musings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $musing = Musing::findOrFail($id)->load('images');
        return view('backend.dashboard.musing.edit', compact('musing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());
        try {
            DB::beginTransaction();


            $musing = Musing::findOrFail($id)->load('images');

            $musingImg = MusingImage::query()->where('musing_id', $musing->id)->delete();

            if (isset($request->image)){
                foreach ($request->image as $value){
//                    if (!empty($musing->images)){
//                        foreach ($musing->images as $imagePath){
//                            $PathForDelete = str_replace('/storage', '', $imagePath->path);
//                            Storage::delete('/public' . $PathForDelete);
//                        }
//                    }
                    $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'musing' . DIRECTORY_SEPARATOR);
                    $name =  Hellper::upload($value, $path);
                    $image = "/storage/musing/" . $name;
//
                    $musingImage = MusingImage::create(
                        [
                            'musing_id' => $musing->id,
                            'path' => $image,
                        ]
                    );

                }
            }

            $musing->update([
                'title' => $request->title,
                'description' => $request->description,
                'status' => isset($request->status) ? 1 : 0,
            ]);


            DB::commit();
            return redirect()->route('musing.index')->with('message', 'Successfully');
        } catch (\Throwable $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $musing = Musing::findOrFail($id);
        $PathForDelete = str_replace('/storage', '', $musing->path);
        Storage::delete('/public' . $PathForDelete);
        $musing->delete();
        return redirect()->route('musing.index')->with('message', 'Successfully');
    }
}
