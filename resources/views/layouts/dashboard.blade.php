<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @yield('title')
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('backend.components.css')
    @yield('css')


</head>
<body class="hold-transition sidebar-mini layout-fixed D.K.">
{{--@include('backend.components.loader')--}}
<div class="wrapper">

    <!-- Navbar -->
    @include('backend.components.navbar')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
   @include('backend.components.sidebar')

   @include('backend.components.flashMessage')
    <!-- Content Wrapper. Contains page content -->
   @yield('dashboard')


   @include('backend.components.footer')
   @include('backend.components.js')
   @yield('js')
</div>
<!-- ./wrapper -->

</body>
</html>
