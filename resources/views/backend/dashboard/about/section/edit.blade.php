@extends('layouts.dashboard')


@section('title')
    <title>Edit</title>
@endsection

@section('css')
@endsection

@section('dashboard')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-12">
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">Section</h3>
                        </div>
                        <form action="{{route('section.update', $section->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="card-body row">
                                @if(isset($section->path))
                                    <div class="form-group col-12">
                                        <div class="form-group col-12">
                                            <label for="exampleInputFile">First Images</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file"  class="custom-file-input" name="image" id="imgfilesOne">
                                                    <label class="custom-file-label" for="imgfilesOne">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="img-view-one">

                                        </div>
                                        <div>
                                            <img src="{{asset($section->path)}}" alt="" style="width: 30%;">
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group col-6">
                                    <label>Title</label>
                                    <input class="form-control" type="text" name="title" placeholder="Slug title" value="{{ $section->title }}">
                                </div>
                                @if(isset($section->sub_title))
                                    <div class="form-group col-6">
                                        <label>Sub Title</label>
                                        <input class="form-control" type="text" name="description" placeholder="Slug title"  value="{{ $section->sub_title }}">
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label>View</label>
                                    <input class="form-control" type="checkbox" name="status" @if($section) checked @endif>
                                </div>
                                <div class="form-group col-12">
                                    <label>Description</label>
                                    <textarea class="summernote" name="description">{{ $section->description }}</textarea>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('js')

@endsection


