@extends('layouts.dashboard')


@section('title')
    <title>Slider</title>
@endsection

@section('css')

    <style>
        .custom-scroll{
            display: block;
            height: 100px !important;
            overflow-y: scroll;
        }
    </style>
@endsection



@section('dashboard')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Slider</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                            <li class="breadcrumb-item active">About slider</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div>
                                <a href="{{route('slider.create')}}" class="btn btn-success" style="float: right; margin: 15px">Create</a>
                            </div>
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Image</th>
                                        <th>Access</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($sliders as $slider)
                                            <tr>
                                                <td>{{$slider->id}}</td>
                                                <td style="width: 10%;"><img src="{{asset($slider->path)}}" alt="" style="width: 100%;"></td>
                                                <td>
                                                    <div class="d-flex">
                                                        <a style="padding: 0px 5px;margin: 0px 8px;" href="{{ route("slider.edit", $slider->id) }}" class="btn btn-primary edit-access"><i  class="fas fa-edit" ></i></a>
                                                        <form method="POST" action="{{ route("slider.destroy" , $slider->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <a href="javascript:;" onclick="return confirm('Are you sure you want to delete this item?');">
                                                                <button type="submit" style="padding: 0px 5px;" class="ml-2 btn btn-danger" >
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </a>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <div style="margin: 0 auto">
                                {{ $sliders->links('pagination::bootstrap-4') }}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')


@endsection
