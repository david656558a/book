@extends('layouts.dashboard')


@section('title')
    <title>Musings</title>
@endsection

@section('css')

    <style>
        .custom-scroll{
            display: block;
            height: 100px !important;
            overflow-y: scroll;
        }
    </style>
@endsection



@section('dashboard')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Books</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                            <li class="breadcrumb-item active">Musings</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>Sub Title</th>
                                        <th>Description</th>
                                        <th>View</th>
                                        <th>Access</th>
                                    </tr>
                                    </thead>
{{--                                    {{dd($musings)}}--}}
                                    <tbody>
                                        @foreach($musings as $musing)
                                            <tr>
                                                <td>{{$musing->id}}</td>
                                                <td style="width: 10%;">
                                                    @if(isset($musing->images[0]->path))
                                                        <span style="    display: flex;overflow: auto;">
                                                            @foreach($musing->images as $val)
                                                                <img src="{{asset($val->path)}}" alt="" style="width: 100%; margin: 5px">
                                                            @if($musing->section == 6)
                                                                <video width="320" height="240" controls >
                                                                  <source src="{{asset($val->path)}}" type="video/mp4">
                                                                  <source src="{{asset($val->path)}}" type="video/ogg">
                                                                Your browser does not support the video tag.
                                                                </video>
                                                            @endif
                                                            @endforeach
                                                         </span>
                                                        @else
                                                        --
                                                    @endif
                                                </td>
                                                <td>{{$musing->title ?? '--'}}</td>
                                                <td>{{$musing->sub_title  ?? '--'}}</td>
                                                <td>{!! $musing->description  ?? '--' !!}</td>
                                                <td>{{$musing->status ? 'View' : 'Hidden'}}</td>
                                                <td>
                                                    <div class="d-flex">
                                                        <a style="padding: 0px 5px;margin: 0px 8px;" href="{{ route("musing.edit", $musing->id) }}" class="btn btn-primary edit-access"><i  class="fas fa-edit" ></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <div style="margin: 0 auto">
                                {{ $musings->links('pagination::bootstrap-4') }}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')


@endsection
