@extends('layouts.dashboard')


@section('title')
    <title>Edit</title>
@endsection

@section('css')
@endsection

@section('dashboard')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-12">
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">Musing</h3>
                        </div>
                        <form action="{{route('musing.update', $musing->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="card-body row">
                                <div class="card-body row">
                                    @if(!isset($musing->title))
                                    <div class="form-group col-12">
                                        <div class="form-group col-12">
                                            <label for="exampleInputFile">First Images</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file"  class="custom-file-input" name="image[]" id="imgfilesOne" @if(in_array($musing->section, [4 ,5]) ) multiple @endif>
                                                    <label class="custom-file-label" for="imgfilesOne">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="img-view-one">

                                        </div>
                                        <div class="custom-video">

                                        </div>
                                        <div>

                                            @if(isset($musing->images[0]->path))
{{--                                                {{dd($musing)}}--}}
                                                @foreach($musing->images as $item)
                                                    <img src="{{asset($item->path)}}" alt="" style="width: 30%;">
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                    @if(isset($musing->title))
                                        <div class="form-group col-6">
                                            <label>Title</label>
                                            <input class="form-control" type="text" name="title" placeholder="Slug title" value="{{ $musing->title }}">
                                        </div>
                                    @endif
                                    @if(isset($musing->sub_title))
                                        <div class="form-group col-6">
                                            <label>Sub title</label>
                                            <input class="form-control" type="text" name="sub_title" placeholder="Slug author"  value="{{$musing->sub_title }}">
                                        </div>
                                    @endif
                                    @if(isset($musing->description))
                                        <div class="form-group col-12">
                                            <label>Description</label>
                                            <textarea class="summernote" name="description">{{ $musing->description }}</textarea>
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label>View</label>
                                        <input class="form-control" type="checkbox" name="status" @if($musing->status) checked @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('js')
    <script>
        $(document).on('change', '.custom-file-input', function () {
            console.log( $(this))
            let name = $(this).val().replace(/.*(\/|\\)/, '')
            $(this).closest('.custom-file').find('.custom-file-label').html(name)

            if(this.files[0].type == 'video/mp4' || this.files[0].type == 'video/ogg'){
                $('#img-view-one').html('')
                $('.custom-video').html(`
                <br>
                <video width="320" height="240" controls>
                  <source src="${window.URL.createObjectURL(this.files[0])}" type="video/mp4">
                  <source src="${window.URL.createObjectURL(this.files[0])}" type="video/ogg">
                Your browser does not support the video tag.
                </video>
            `)
            }else{

            }

        })
    </script>
@endsection


