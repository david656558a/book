@extends('layouts.dashboard')


@section('title')
    <title>Create</title>
@endsection

@section('css')

@endsection



@section('dashboard')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Create</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>


    <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <h3 class="card-title">Book</h3>
                            </div>
                            <form action="{{route('book.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body row">
                                    <div class="card-body row">
                                        <div class="form-group col-12">
                                            <div class="form-group col-12">
                                                <label for="exampleInputFile">First Images</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file"  class="custom-file-input" name="image" id="imgfilesOne">
                                                        <label class="custom-file-label" for="imgfilesOne">Choose file</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="img-view-one">

                                            </div>
                                        </div>
                                        <div class="form-group col-6">
                                            <label>Title</label>
                                            <input class="form-control" type="text" name="title" placeholder="Slug title" value="{{ old('title') }}">
                                        </div>
                                        <div class="form-group col-6">
                                            <label>Author</label>
                                            <input class="form-control" type="text" name="author" placeholder="Slug author"  value="{{ old('author') }}">
                                        </div>
                                        <div class="form-group col-12">
                                            <label>Sub description</label>
                                            <textarea class="summernote" name="sub_description">{{ old('sub_description') }}</textarea>
                                        </div>
                                        <div class="form-group col-12">
                                            <label>Description</label>
                                            <textarea class="summernote" name="description">{{ old('description') }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>View</label>
                                            <input class="form-control" type="checkbox" name="status" checked>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
    <!-- /.content -->
</div>
@endsection

@section('js')
    <script>
        $(document).on('change', '.custom-file-input', function () {
            let name = $(this).val().replace(/.*(\/|\\)/, '')
            $(this).closest('.custom-file').find('.custom-file-label').html(name)

            $(this).closest('.custom-file').closest('.input-group').closest('.form-group').find('.custom-audio').html(`
                <br>
                <audio controls>
                    <source src="${window.URL.createObjectURL(this.files[0])}" type="audio/ogg">
                    <source src="${window.URL.createObjectURL(this.files[0])}" type="audio/mpeg">
                </audio>
            `)
        })
    </script>

@endsection


