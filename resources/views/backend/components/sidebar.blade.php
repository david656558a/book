<aside class="main-sidebar main-sidebar sidebar-dark-primary elevation-4" > <!--  style="position: absolute;"  ---->
    <!-- Brand Logo -->
    <a href="/admin" class="brand-link">
        <img src="{{asset('assets/backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Admin panel</span>
    </a>

    <style>
        .nav-treeview li{
            display: block;
            margin-left: 10px;
        }

    </style>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{route('user.edit', 1)}}" class="nav-link {{ Route::currentRouteNamed(['user.edit']) ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>
                            User
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview {{ Route::currentRouteNamed(['slider.index', 'slider.create', 'slider.edit', 'section.index', 'section.edit']) ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ Route::currentRouteNamed(['slider.index', 'slider.create', 'slider.edit', 'section.index', 'section.edit']) ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>
                            About
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('slider.index')}}" class="nav-link {{ Route::currentRouteNamed(['slider.index', 'slider.create', 'slider.edit']) ? 'active' : '' }}">
                                <i class="far fa-dot-circle nav-icon"></i>
                                <p>Slider</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('section.index')}}" class="nav-link {{ Route::currentRouteNamed(['section.index', 'section.edit']) ? 'active' : '' }}">
                                <i class="far fa-dot-circle nav-icon"></i>
                                <p>Section</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{route('book.index')}}" class="nav-link {{ Route::currentRouteNamed(['book.index', 'book.create', 'book.edit']) ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>
                            Books
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('musing.index')}}" class="nav-link {{ Route::currentRouteNamed(['musing.index', 'musing.edit']) ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>
                            Musing
                        </p>
                    </a>
                </li>

                <li> <br><br> </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
