@extends('layouts.app')

@section('content')
    <section class="first-section">
        <div class="slider">
            @foreach($sliders as $slider)
                <div class="slider-item"><img src="{{asset($slider->path)}}" alt="slider"></div>
            @endforeach
        </div>
    </section>

    @foreach($sections as $section)
{{--        {{dd($section)}}--}}
        @if($section->section == 1)
        <section class="about-section">
            <div class="content">
                <div class="text">
                    <div class="author-info" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s">
                        <div class="name">{{$section->title}}</div>
                        <div class="speciality">
                            {{$section->sub_title}}
                        </div>
                        <div class="line"></div>
                        <div class="description">
                            {{$section->description}}
                        </div>
                    </div>
                </div>
                <div class="image" data-aos="fade-left" data-aos-duration="2000" data-aos-delay="2s"><img class="" src="{{asset($section->path)}}" alt="image"></div>
            </div>
        </section>
        @endif
        @if($section->section == 2)
        <section class="third-section">
            <div class="content" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s">
                <div class="title">{{$section->title}}</div>
                <div class="description">{{$section->description}}</div>
                <span class="author">Wiliam Stafford</span>
            </div>
        </section>
        @endif
        @if($section->section == 3)
        <section class="forth-section">
            <div class="content">
                <div class="image" data-aos="fade-right" data-aos-duration="2000" data-aos-delay="2s"><img src="{{asset($section->path)}}" alt="image"></div>
                <div class="text">
                    <div class="text-info" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s">
                        <div class="title">{{$section->title}}</div>
                        <div class="description">
                            <p>{{$section->description}}</p>
{{--                            <p>My life is an endeavor to follow that thread.</p>--}}
{{--                            <p>I specialize in following it where not many dare to tread. I want to know life biblically, the way a man knows a woman (or other configuration of such). I want to know the water by getting wet. Theory, commandments, concepts leave me empty and not the good kind of emptiness</p>--}}
{{--                            <p>My driving question is, “is that true?”. Is it true wholly? Where and how is it true? For whom is it true and why? Can it withstand the test of time? Is it true for me as a woman? The last one has taken me into some very off beaten paths. Givens are often no longer givens when I ask this question. The world turns upside down.</p>--}}
{{--                            <p>My two guiding principles are the ideas that “I’ve come only for this”, whatever is presented before me. It is mine to puzzle, to play, to explore and finally, to love. Which leads me to the how I explore which is to ask, Can I love this? Can I love even this? Who is the I who is loving in this moment? What does love look like here? Does it require a peaceful approach, approval, power, some good old fashioned wrath? And then, what is this? I must leave who I believe myself to be to answer that question. What is this on “its” terms and not on mine. As a free woman I want all things to be free, liberated from any ideas I would impose on them.</p>--}}

                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endif
        @if($section->section == 4)
        <section class="fifth-section">
            <div class="content" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s">
                <div class="title">{{$section->title}}</div>
                <div class="description" >
                    <p>{{$section->description}}</p>
{{--                    <p>I ask what is the highest form of this? What does this need to become that? What does it need cut away in order to become its own unique beauty? How will we know when it is realized?</p>--}}
{{--                    <p>My absolute specialty is doing this with shadow material, with what we discard or disregard, what we see as trash, people we see as monsters, groups we have seen as outsiders. The mark of my work is that something that has been denigrated is now exalted and not by assimilation but by becoming wholly and uniquely itself -and then fitting into the culture in such a way that it is viewed as invaluable.</p>--}}
{{--                    <p>I do this by turning every process into a spiritual journey. Or, as one reporter described it, I do everything I do “religiously”. I do it agnostically as well meaning, I am not constrained by the edicts of my known world. I will enter any domain, apprehend it, taste, touch, smell, feel and most of all listen to it until it reveals it’s secrets and then I will offer my gifts to it on it’s terms. That part is vital, that it is not me imposing my will, my idea of what it wants to be but that I am using my powers to access the resources for it to become itself.</p>--}}

                </div>

            </div>
        </section>
        @endif
        @if($section->section == 5)
        <section class="sixth-section">
            <div class="content">
                <div class="text">
                    <div class="text-info" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s">
                        <div class="title">{{$section->title}}</div>
                        <div class="description">
                            {{$section->description}}
{{--                            <p>I ask what is the highest form of this? What does this need to become that? What does it need cut away in order to become its own unique beauty? How will we know when it is realized?</p>--}}
{{--                            <p>My absolute specialty is doing this with shadow material, with what we discard or disregard, what we see as trash, people we see as monsters, groups we have seen as outsiders. The mark of my work is that something that has been denigrated is now exalted and not by assimilation but by becoming wholly and uniquely itself -and then fitting into the culture in such a way that it is viewed as invaluable.</p>--}}
{{--                            <p>I do this by turning every process into a spiritual journey. Or, as one reporter described it, I do everything I do “religiously”. I do it agnostically as well meaning, I am not constrained by the edicts of my known world. I will enter any domain, apprehend it, taste, touch, smell, feel and most of all listen to it until it reveals it’s secrets and then I will offer my gifts to it on it’s terms. That part is vital, that it is not me imposing my will, my idea of what it wants to be but that I am using my powers to access the resources for it to become itself.</p>--}}

                        </div>
                    </div>
                </div>
                <div class="image" data-aos="fade-left" data-aos-duration="2000" data-aos-delay="2s"><img src="{{asset($section->path)}}" alt="image"></div>
            </div>
        </section>
        @endif
    @endforeach

@endsection
