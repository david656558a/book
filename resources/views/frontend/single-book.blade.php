@extends('layouts.app')

@section('content')

    <section class="single-book-info">
        <div class="content">
            <div class="image" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s"><img src="{{asset($book->path)}}" alt="book image"></div>
            <div class="book-info"  data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s">
                <div class="title">{{$book->title}}</div>
                <div class="description">
                    {!! $book->description !!}
                </div>
                <div class="author-name">{{$book->autor}}</div>
            </div>
        </div>
    </section>

    <section class="books-slider more-books">
        <div class="other-books">Other Books</div>
        <div class="book-list">
            @foreach($books as $bookImages)
                <a href="{{route('book-single', $bookImages->id)}}"><div class="book-item"><img src="{{asset($bookImages->path)}}" alt="book" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s"></div></a>
            @endforeach
        </div>
    </section>

@endsection
