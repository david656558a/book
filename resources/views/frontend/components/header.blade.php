<header>
    <div class="navbar">
        <div class="navbar-logo"><img src="{{asset('assets/frontend/images/logo.png')}}" alt="logo"></div>
        <div class="line"></div>
        <div class="navbar-list">
            <div class="navbar-item"><a href="{{route('about')}}" class="{{ Route::currentRouteNamed(['about']) ? 'active' : '' }}">About</a></div>
            <div class="navbar-item"><a href="{{route('books')}}" class="{{ Route::currentRouteNamed(['books']) ? 'active' : '' }}">Books</a ></div>
            <div class="navbar-item"><a href="{{route('musings')}}" class="{{ Route::currentRouteNamed(['musings']) ? 'active' : '' }}">Musings</a></div>
            <div class="navbar-item"><a href="https://www.instagram.com/nicoledaedone/" target="_blank">Instagram</a></div>
        </div>
    </div>
</header>
