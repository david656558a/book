@extends('layouts.app')

@section('content')
    @foreach($musings as $musing)
        @if($musing->section == 1)
        <section class="first-section">
            @foreach($musing->images as $section1)
            <div class="main-image" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s">
                <img src="{{asset($section1->path)}}" alt="image">
            </div>
            @endforeach
        </section>
        @endif
        @if($musing->section == 2)
            <section class="about-section">
                <div class="content">
                    <div class="text">
                        <div class="author-info" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s">
                            <div class="date">{{$musing->sub_title}}</div>
                            <div class="line"></div>
                            <div class="description">
                                {!! $musing->description !!}
                            </div>
                        </div>
                    </div>
                    @foreach($musing->images as $section2)
                        <div class="image " data-aos="fade-left" data-aos-duration="2000" data-aos-delay="2s"><img class="" src="{{asset($section2->path)}}" alt="image"></div>
                    @endforeach
                </div>
            </section>
        @endif
        @if($musing->section == 3)
        <section class="third-section">
            <div class="content" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s">
                <div class="title">{{$musing->title}}</div>
                <div class="description">{!! $musing->description !!}</div>
{{--                <span class="article-date">JANUARY 31, 2023 | ARTICLE</span>--}}
            </div>
        </section>
        @endif
        @if($musing->section == 4)
        <section class="forth-section">
            <div class="content">
                @foreach($musing->images as $key => $section4)
                    <div class="@if($key == 0) first-image @else second-image @endif " data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s"><img src="{{asset($section4->path)}}" alt="book image"></div>
                @endforeach
{{--                <div class="second-image" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s"><img src="{{asset('/assets/frontend/images/book-description.png')}}" alt="book description"></div>--}}
            </div>
        </section>
        @endif
        @if($musing->section == 5)
        <section class="musings-fifth-section">
            <div class="content">
                <div class="images-content">
                    @foreach($musing->images as $key => $section5)
                        <div class="@if($key == 0) firstImage @else secondImage @endif" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s"><img src="{{asset($section5->path)}}" alt="image"></div>
                    @endforeach
{{--                    <div class="secondImage" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s"><img src="{{asset('/assets/frontend/images/book-image.png')}}" alt="image"></div>--}}
                </div>
            </div>
        </section>
        @endif
        @if($musing->section == 6)
        <section class="video-section">
            <div class="video-image">
            <video  controls style="width: 100%;">
                @foreach($musing->images as $key => $section6)
                    <source src="{{asset($section6->path)}}" type="video/mp4">
                @endforeach
            </video>
            </div>
{{--            <div class="video-image" onclick="lightbox_open();">--}}
{{--                <video  controls style="width: 100%;">--}}
{{--                    @foreach($musing->images as $key => $section6)--}}
{{--                        <source src="{{asset($section6->path)}}" type="video/mp4">--}}
{{--                    @endforeach--}}
{{--                </video>--}}
{{--                <img src="{{asset('assets/frontend/images/video-image.png')}}" alt="video image">--}}
{{--            </div>--}}
{{--            <div class="video-content" id="light">--}}
{{--                <video id="video" controls>--}}
{{--                    @foreach($musing->images as $key => $section7)--}}
{{--                        <source src="{{asset($section7->path)}}" type="video/mp4">--}}
{{--                    @endforeach--}}
{{--                </video>--}}
{{--                <a class="boxclose" onclick="lightbox_close();"></a>--}}
{{--            </div>--}}

{{--            <div id="fade" onClick="lightbox_close();"></div>--}}
        </section>
        @endif
    @endforeach


@endsection
