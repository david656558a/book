@extends('layouts.app')

@section('content')
    <section class="books-slider">
        <div class="book-list">
            @foreach($books as $bookImages)
                <a href="{{route('book-single', $bookImages->id)}}"><div class="book-item"><img src="{{asset($bookImages->path)}}" alt="book" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s"></div></a>
            @endforeach
        </div>
    </section>

    <section class="books-info">
        <div class="content">
            <div class="left-side" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s">
                @foreach($books as $kay => $bookTitle)
                    <div class="book-title " onclick="openCity('book{{$kay}}')">{{$bookTitle->title}}</div>
                @endforeach
            </div>
            <div class="right-side">
                @foreach($books as $kay => $bookSub)
                <div id="book{{$kay}}" class="book-description" data-aos="fade-in" data-aos-duration="2000" data-aos-delay="2s" @if($kay != 0) style="display:none" @endif>
                    <h2>{{$bookSub->title}}</h2>
                    <p>{!! $bookSub->sub_description !!}</p>
                    <div class="book-description-footer">
                        <span class="book-author">{{$bookSub->autore}}</span>
                        <a href="{{route('book-single', $bookSub->id)}}" class="read-more">Read More
                            <img src="{{asset('assets/frontend/images/next-slide-active.png')}}" alt="arrow">
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>


    </section>


@endsection
