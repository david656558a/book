$('#imgfiles').on('change', function (e) {
    renderImages();
})
$('#img-view').on('click', '.removeImage', function () {
    let remIndex = Number($(this).attr('data-value'));
    let dt = new DataTransfer();
    for (let i=0; i<$('#imgfiles')[0].files.length; i++){
        if (i !== remIndex){
            dt.items.add($('#imgfiles')[0].files[i]);
        }
    }
    $('#imgfiles')[0].files = dt.files;
    renderImages();
})
function renderImages(){
    let el = $('#img-view');
    el.html(``);
    for (let i=0; i<$('#imgfiles')[0].files.length; i++){
        let src = URL.createObjectURL($('#imgfiles')[0].files[i])
        el.append(`
                    <div style="display:flex; float: left; margin: 10px; position:relative">
                         <div class="removeImage" data-value="${i}" style="position:absolute; top: 5px; background: #0c0c0c85; left: 5px; border: 1px solid red; cursor: pointer;width: 29px; height: 29px; text-align: center; color:red">x</div>
                         <img style="width: 160px;" src="${src}">
                    </div>
                `);
    }
}

// ====================================================== ID ========================================================

$('#imgfilesOne').on('change', function (e) {
    renderImagesOne();
})
$('#img-view-one').on('click', '.removeImageOne', function () {
    let remIndex = Number($(this).attr('data-value'));
    let dt = new DataTransfer();
    for (let i=0; i<$('#imgfilesOne')[0].files.length; i++){
        if (i !== remIndex){
            dt.items.add($('#imgfilesOne')[0].files[i]);
        }
    }
    $('#imgfilesOne')[0].files = dt.files;
    renderImagesOne();
})
function renderImagesOne(){
    let el = $('#img-view-one');
    el.html(``);
    for (let i=0; i<$('#imgfilesOne')[0].files.length; i++){
        let src = URL.createObjectURL($('#imgfilesOne')[0].files[i])
        el.append(`
                    <div style="display:flex; float: left; margin: 10px; position:relative">
                         <div class="removeImageOne" data-value="${i}" style="position:absolute; top: 5px; background: #0c0c0c85; left: 5px; border: 1px solid red; cursor: pointer;width: 29px; height: 29px; text-align: center; color:red">x</div>
                         <img style="width: 160px;" src="${src}">
                    </div>
                `);
    }
}


// ==============================================================================================================

// =================================================== CLASS ===========================================================

$(document).on('click', '.custom-remove-info', function () {
    if (confirm('Are you sure you want to save this thing into the database?')) {
        // Save it!
        $(this).closest('.card-tools').closest('.card-header').closest('.card-default').remove()
    } else {
        // Do nothing!
        console.log('Thing was not saved to the database.');
    }
})

$(document).on('change', '.img-files-one-custom-input', function (e) {
    renderImagesOneCustom($(this));
})
$(document).on('click', '.remove-image-one-custom', function () {
    let remIndex = Number($(this).attr('data-value'));
    let inputFile = $(this).closest('.closest-img-files-one-custom').find('.img-files-one-custom-input');
    console.log(inputFile)
    let dt = new DataTransfer();
    for (let i=0; i<inputFile[0].files.length; i++){
        if (i !== remIndex){
            dt.items.add(inputFile[0].files[i]);
        }
    }
    inputFile[0].files = dt.files;
    renderImagesOneCustom(inputFile);
})
function renderImagesOneCustom(file){
    let el = file.closest('.closest-img-files-one-custom').find('.img-view-one-custom');
    if (!el.length){
        console.log('please add this class in main div closest-img-files-one-custom')
    }
    el.html(``);
    for (let i=0; i<file[0].files.length; i++){
        let src = URL.createObjectURL(file[0].files[i])
        el.append(`
                    <div style="display:flex; float: left; margin: 10px; position:relative">
                         <div class="remove-image-one-custom" data-value="${i}" style="position:absolute; top: 5px; background: #0c0c0c85; left: 5px; border: 1px solid red; cursor: pointer;width: 29px; height: 29px; text-align: center; color:red">x</div>
                         <img style="width: 160px;" src="${src}">
                    </div>
                `);
    }
}


// ==================================================== ID ==========================================================



$(document).on('change', '#imgfilesOneHeader',function (e) {
    renderImagesOneHeader();
})
$(document).on('click', '.removeImageOneHeader', function () {
    let remIndex = Number($(this).attr('data-value'));
    let dt = new DataTransfer();
    for (let i=0; i<$('#imgfilesOneHeader')[0].files.length; i++){
        if (i !== remIndex){
            dt.items.add($('#imgfilesOneHeader')[0].files[i]);
        }
    }
    console.log($('#imgfilesOneHeader')[0].files);
    $('#imgfilesOneHeader')[0].files = dt.files;

    renderImagesOneHeader();
})
function renderImagesOneHeader(){
    let el = $('#img-view-one-header');
    el.html(``);
    for (let i=0; i<$('#imgfilesOneHeader')[0].files.length; i++){
        let src = URL.createObjectURL($('#imgfilesOneHeader')[0].files[i])
        let type = $('#imgfilesOneHeader')[0].files[0].type;
        console.log(type.split('/'))
        if (type.split('/')[0] == 'video'){
            el.append(`
                    <div style="display:flex; float: left; margin: 10px; position:relative">
                         <div class="removeImageOneHeader" data-value="${i}" style="position:absolute; top: 5px; background: #0c0c0c85; left: 5px; border: 1px solid red; cursor: pointer;width: 29px; height: 29px; text-align: center; color:red; z-index: 999999">x</div>
                         <video width="400" controls style="width: 160px;">
                             <source src="${src}" type="${type}">
                         </video>
                    </div>
                `);
        }else{
            el.append(`
                    <div style="display:flex; float: left; margin: 10px; position:relative">
                         <div class="removeImageOneHeader" data-value="${i}" style="position:absolute; top: 5px; background: #0c0c0c85; left: 5px; border: 1px solid red; cursor: pointer;width: 29px; height: 29px; text-align: center; color:red">x</div>
                         <img style="width: 160px;" src="${src}">
                    </div>
                `);
        }

    }
}

// ==============================================================================================================


// ====================================================== VOICE ========================================================



$(document).on('click', '.remove-voice', function () {
    let inputFile = $(this).closest('.custom-audio').closest('.form-group').find('.custom-file-input');
    let dt = new DataTransfer();
    inputFile[0].files = dt.files;
    $(this).closest('.custom-audio').html('')
    voice(inputFile);
})

$(document).on('change', '.custom-file-input', function () {
    console.log($(this))
    voice($(this));

})

function voice(thisVal) {
    let name = thisVal.val().replace(/.*(\/|\\)/, '')
    thisVal.closest('.custom-file').find('.custom-file-label').html(name)
    thisVal.closest('.custom-file').closest('.input-group').closest('.form-group').find('.custom-audio').attr('style', 'position: relative')
    console.log(thisVal);
    thisVal.closest('.custom-file').closest('.input-group').closest('.form-group').find('.custom-audio').html(`
                <br>
                <div class="remove-voice" data-value="1" style=" z-index:1; position:absolute;top: 36px;right: 19px; background: #0c0c0c85; border: 1px solid red; cursor: pointer;width: 29px; height: 29px; text-align: center; color:red">x</div>
                <audio controls>
                    <source src="${window.URL.createObjectURL(thisVal[0].files[0])}" type="audio/ogg">
                    <source src="${window.URL.createObjectURL(thisVal[0].files[0])}" type="audio/mpeg">
                </audio>
            `)
}
