$('.del-clic').on('click', function () {
    var id = $(this).attr('data-id');
    $.ajax({
        type: 'POST',
        url: "/admin/destination/delete/image/" + id,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name = "csrf-token"]').attr('content')
        },
        data: {
            id:id,
        } ,
        success: function(result){
            console.log(result.id);
            $('[data-id="'+ result.id +'"]').remove();
        }
    });
})


