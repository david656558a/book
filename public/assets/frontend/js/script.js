$('.slider').slick({
    infinite: true,
    autoplay: true,
    autoplaySpeed: 1200,
    slidesToShow: 1,
    fade: true,
    arrow: false,
    dots: false,
    speed: 1500

});

$('.slider').on("mouseup",".slick-slide", function () {
    $('.slider').slick("slickNext");
});

$('.book-list').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 1465,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 993,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});

function openCity(bookName) {
    var i;
    var x = document.getElementsByClassName("book-description");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    document.getElementById(bookName).style.display = "block";
}

$(".book-title").click(function() {
    // remove classes from all
    $(".book-title").removeClass("active");
    // add class to the one we clicked
    $(this).addClass("active");
});


//video playing lightbox
window.document.onkeydown = function(e) {
    if (!e) {
        e = event;
    }
    if (e.keyCode == 27) {
        lightbox_close();
    }
}
function lightbox_open() {
    var lightBoxVideo = $("#video");
    window.scrollTo(0, 0);
    $('.video-content').css("display","block");
    $('#fade').css("display","block");
    $("html, body").css("overflow","hidden");
    lightBoxVideo[0].play();
}

function lightbox_close() {
    var lightBoxVideo = $("#video");
    $('.video-content').css("display","none");
    $('#fade').css("display","none");
    $("html, body").css("overflow","auto");
    lightBoxVideo.get(0).pause();
}


AOS.init();